This is the official repository for the PA197 Adaptive Cybersecurity Game project.

Please check the [wiki on the sidebar](https://gitlab.fi.muni.cz/xpavela/pa197-adaptive-game-2023/-/wikis/home) for more information on what to do.