CREATE TABLE intusers (uname varchar(30), passwd varchar(30));
LOAD DATA INFILE '/var/lib/mysql/users.csv'
INTO TABLE intusers
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 0 ROWS;
