CREATE TABLE ranks (surname varchar(30), first_name varchar(30), army_rank varchar(50));
LOAD DATA INFILE '/var/lib/mysql/ranks.csv'
INTO TABLE ranks
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 0 ROWS;
