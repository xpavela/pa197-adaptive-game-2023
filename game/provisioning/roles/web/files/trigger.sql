DELIMITER //

CREATE TRIGGER promotion BEFORE UPDATE ON ranks FOR EACH ROW BEGIN IF old.surname = 'Prochazka' AND new.army_rank = 'Captain' THEN SET new.army_rank = 'Captain [FLAG: RandomEmojiGenerator]'; END IF; END;//

DELIMITER ;
