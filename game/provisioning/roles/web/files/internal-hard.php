<!DOCTYPE html>
<html>
<head>
	<title>Internal Login Page</title>
</head>

<body>
	<div align = "center">
		<h2><br>Internal page of the army of Slovakistan</h2>
		<p>Unauthorized access is prohibited by law<p>
		<p>Password problem fixed<p>
		<br>

		<div style = "width:300px; border: solid 1px #33333; " align = "left">
			<form action = "" method = "post">
				<label>Username  </label><input type="text" name="username"> <br><br>
				<label>Password  </label><input type="text" name="password"> <br><br>
				<input type="submit" value=" Submit "/><br>
			</form>

			<div style = "font-size:11px; color:#cc0000; margin-top:10px"><?php echo $error; ?></div>
		</div>

	</div>
</body>
</html>

<?php
include("database-config.php");

$db = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);

if (!$db) {
	die("Could not connect to DB");
}

if (isset($_POST['username'], $_POST['password'])) {
	$username = $_POST['username'];
	$password = $_POST['password'];


	$query = $db->prepare("SELECT * FROM intusers WHERE uname = '$username' AND passwd = ?");
	$query->bind_param("s", $password);
	$query->execute();

	$result = $query->get_result();
	$count = mysqli_num_rows($result);
	if ($count == 1) {
		header("location: filemanager.php");
		exit();
	} else {
		$error = "Invalid credentials";
	}
}
mysqli_close($db);
?>
